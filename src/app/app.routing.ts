import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { CalcComponent } from './calc/calc.component';
import { SearchComponent } from './search/search.component'; 
import { useradminComponent } from './useradmin/useradmin.component';
import { HomeComponent } from './home/home.component';
import { AddAlertComponent } from './alertadd/alertadd.component';
import { NotFoundComponent } from './notfound/notfound.component';

export const AppRoutes: Routes = [
	{ path: '', component: HomeComponent },
    { path: 'calc', component: CalcComponent },
    { path: 'search', component: SearchComponent },
    { path: 'useradmin', component: useradminComponent },
    { path: 'alertadd', component: AddAlertComponent }

    { path: '**', component: NotFoundComponent }
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ROUTING } from './app.routing'
import { AppComponent } from './app.component';
import { CalcComponent } from './calc/calc.component';
import { SearchComponent } from './search/search.component';
import { useradminComponent } from './useradmin/useradmin.component';
import { HomeComponent } from './home/home.component';
import { AddAlertComponent } from './alertadd/alertadd.component';
import { NotFoundComponent } from './notfound/notfound.component';
import { FormsModule } from '@angular/forms';
 
@NgModule({
  declarations: [
    AppComponent,
    CalcComponent,
    HomeComponent,
    SearchComponent,
    useradminComponent,
    AddAlertComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

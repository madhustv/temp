import { Component } from '@angular/core';
import { User } from './country';

@Component({
  selector: 'useradmin',
  templateUrl: 'useradmin.component.html',
  styleUrls: ['useradmin.component.css']
})
export class useradminComponent {
	selectedCountry:User = new User(1, 'Madhu');
	  users = [
	     new User(1, 'Mahesh' ),
	     new User(2, 'Rajesh' ),
	     new User(3, 'Suresh' ),
	     new User(4, 'Balaji')
	  ];
   
}

import { Component } from '@angular/core';

@Component({
  selector: 'home',
  templateUrl: 'home.component.html'
})
export class HomeComponent {
    
  appList: any[] = [{
			      "ID": "2400",
			      "Provider": 'IBAS',
			      "Service": 'acountDomainServices',
			      "Operation": 'performfullhealthcheck',
			      "Template": 'v210'
			   },
			   {
			     "ID": "2401",
			      "Provider": 'IBCR',
			      "Service": 'acountInfo',
			      "Operation": 'iconnect',
			      "Template": '42104'
			   }];
}

import { Component } from '@angular/core';
import { Alertsdata } from './alerts';

@Component({
  selector: 'alertadd',
  templateUrl: 'alertadd.component.html',
  styleUrls: ['alertadd.component.css']
})
export class AddAlertComponent {
	selectedCountry:Alertsdata = new Alertsdata(1, 'alert1');
	  alertslist = [
	     new Alertsdata(1, 'alert1' ),
	     new Alertsdata(2, 'alert2' ),
	     new Alertsdata(3, 'alert3' ),
	     new Alertsdata(4, 'alert4')
	  ];
   
}
